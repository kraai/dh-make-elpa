package DhMakeELPA::Config;

use strict;
use warnings;

our $VERSION = '0.1.0';

use base 'DhMakePerl::Config';

use Getopt::Long;

=head1 NAME

DhMakeELPA::Config - dh-make-elpa configuration class

=cut

my @OPTIONS = ( 'pkg-emacsen!', 'version=s' );

my @COMMANDS = ( 'make', 'help' );

__PACKAGE__->mk_accessors(
    do {
        my @opts = ( @OPTIONS, @COMMANDS );
        for (@opts) {
            s/[=:!|].*//;
            s/-/_/g;
        }
        @opts;
    },
    'command',
    '_explicitly_set',
);

use constant DEFAULTS => {
                          dh            => 12,
                          data_dir      => '/usr/share/dh-make-elpa',
                          home_dir      => "$ENV{HOME}/.dh-make-elpa",
                          source_format => '3.0 (quilt)',
                         };

=head1 METHODS

=over

=item parse_command_line_options()

Parses command line options and populates object members.

=cut

sub parse_command_line_options {
    my $self = shift;

    Getopt::Long::Configure( qw( pass_through no_auto_abbrev no_ignore_case ) );
    my %opts;
    GetOptions( \%opts, @OPTIONS )
      or die "Error parsing command-line options\n";

    while ( my ( $k, $v ) = each %opts ) {
        my $field = $k;
        $field =~ s/-/_/g;
        $self->$field( $opts{$k} );
        $self->_explicitly_set->{$k} = 1;
    }

    %opts = ();
    Getopt::Long::Configure('no_pass_through');
    GetOptions( \%opts, @COMMANDS )
        or die "Error parsing command-line options\n";

    if (%opts) {
        my $cmd = ( keys %opts )[0];
        warn "WARNING: double dashes in front of sub-commands are deprecated\n";
        warn "WARNING: for instance, use '$cmd' instead of '--$cmd'\n";
    }
    else {
        my %cmds;
        for (@COMMANDS) {
            my $c = $_;
            $c =~ s/\|.+//;     # strip short alternatives
            $cmds{$c} = 1;
        }

        # treat the first non-option as command
        # if it looks like one
        $opts{ shift(@ARGV) } = 1
            if $ARGV[0]
                and $cmds{ $ARGV[0] };

        # by default, create source package
        $opts{make} = 1 unless %opts;
    }

    if ( scalar( keys %opts ) > 1 ) {
        die "Only one of " .
            join(', ', @COMMANDS ) . " can be specified\n";
    }

    $self->command( ( keys %opts )[0] );
}

=back

=head1 COPYRIGHT & LICENSE

=over

=item Copyright (C) 2016-2018 Sean Whitton <spwhitton@spwhitton.name>

=item Copyright (C) 2018 Lev Lamberov <dogsleg@debian.org>

=back

It is heavily based on the corresponding part of dh-make-perl, which
is originally copyrighted as follows:

=over

=item Copyright (C) 2008-2010 Damyan Ivanov <dmn@debian.org>

=item Copyright (C) 2009-2010 Gregor Herrmann <gregoa@debian.org>

=item Copyright (C) 2009 Ryan Niebur <RyanRyan52@gmail.com>

=back

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License version 2 as published by the Free
Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 51 Franklin
Street, Fifth Floor, Boston, MA 02110-1301 USA.

=cut

1;
