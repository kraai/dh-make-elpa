package DhMakeELPA::Command::help;

=head1 NAME

DhMakeELPA::Command::help - dh-make-elpa help implementation

=head1 DESCRIPTION

This module implements the I<help> command of L<dh-make-elpa(1)>.

=cut

use strict; use warnings;

use base 'DhMakeELPA';
use Pod::Usage;

=head1 METHODS

=over

=item execute

Provides I<help> command implementation.

=cut

sub execute {
    my $self = shift;

    # Return SYNOPSIS and die referencing the full manpage
    die pod2usage( -message => "See `man 1 dh-make-elpa' for details.\n" )
}

=back

=cut

1;

=head1 COPYRIGHT & LICENSE

=over

=item Copyright (C) 2018 Lev Lamberov <dogsleg@debian.org>

=back

It is heavily based on the corresponding part of dh-make-perl, which
is originally copyrighted as follows:

=over

=item Copyright (C) 2008, 2009, 2010 Damyan Ivanov <dmn@debian.org>

=back

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License version 2 as published by the Free
Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 51 Franklin
Street, Fifth Floor, Boston, MA 02110-1301 USA.

=cut
